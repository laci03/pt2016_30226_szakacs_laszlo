import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Model.*;

public class Controller {
	private Model model;
	private View view;

	public Controller(Model model_, View view_) {
		this.model = model_;
		this.view = view_;
		this.view.addBtn1(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String userInput = " ";
				try {
					userInput = view.getPolinom1();
					model.setPolinom1(userInput);

				} catch (NumberFormatException nfex) {
				}

			}

		});
		this.view.addBtn2(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String userInput = " ";
				try {
					userInput = view.getPolinom2();
					model.setPolinom2(userInput);
				} catch (NumberFormatException nfex) {
				}

			}

		});

		this.view.addBtnDerivare(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (view.isCheckBox1() == true) {
					model.derivare(0);
					String text = model.getPolinom1();
					view.setPolinom1(text);
				}
				if (view.isCheckBox2() == true) {
					model.derivare(1);
					String text = model.getPolinom2();
					view.setPolinom2(text);
				}
			}

		});
		
		this.view.addBtnIntegrare(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (view.isCheckBox1() == true) {
					model.integrare(0);
					String text = model.getPolinom1();
					view.setPolinom1(text);
				}
				if (view.isCheckBox2() == true) {
					model.integrare(1);
					String text = model.getPolinom2();
					view.setPolinom2(text);
				}
			}

		});
		this.view.addBtnAdunare(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.adunare();
				String text = model.getRezultat();
				view.setRezultat(text);
			}

		});

		this.view.addBtnScadere(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.scadere();
				String text = model.getRezultat();
				view.setRezultat(text);
			}

		});
		
		this.view.addBtnF(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int nr=view.getF();
				if (view.isCheckBox1() == true) {
					String rezultat= model.functie(nr,0);
					view.setF(rezultat);
				}else if (view.isCheckBox2() == true) {
					String rezultat= model.functie(nr,1);
					view.setF(rezultat);
				}
			}
		});

		this.view.addBtnReset1(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String zero = "0";
				if (view.isCheckBox1() == true) {
					model.setPolinom1(zero);
					view.setPolinom1(zero);
				}
				if (view.isCheckBox2() == true) {
					model.setPolinom2(zero);
					view.setPolinom2(zero);
				}
			}
		});

		this.view.addBtnInmultire(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				model.inmultire();
				view.setRezultat(model.getRezultat());

			}

		});
		
		this.view.addBtnImpartire(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				model.impartire();
				view.setRezultat(model.getRezultat());
				view.setRest(model.getRest());

			}

		});

	}

}

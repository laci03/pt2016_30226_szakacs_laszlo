import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Scanner;

import Model.*;

public class Simulare {

	public static void main(String[] args) {
		Model model = new Model();
		View view = new View();
		Controller controller=new Controller(model, view);
		view.setVisible(true);

		ArrayList<Monom> pol=new ArrayList<Monom>();
		pol.add(new Monom(1,2));
		pol.add(new Monom(2,2));
	}

}

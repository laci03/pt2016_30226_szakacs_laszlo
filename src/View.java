import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

public class View extends JFrame {

	private JPanel contentPane;
	private JTextField tf1;
	private JTextField tf2;
	private JButton btn1 = new JButton("Citire pol1");
	private JButton btn2 = new JButton("Citire pol2");
	private JButton btnDerivare;
	private JButton btnAdunare;
	private JButton btnScadere;
	private JButton btnR1;
	private JCheckBox check1;
	private JCheckBox check2;
	private JButton btnInmultire;
	private JTextField tfRezultat;
	private JButton btnIntegrare;
	private JButton btnImpartire;
	private JTextField tfRest;
	private JTextField textField;
	private JButton btnF;

	/**
	 * Create the frame.
	 */

	public View() {
		setTitle("Calculator Polinoame");
		setResizable(false);
		initComponents();
	}

	public void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(350, 200, 527, 295);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		tf1 = new JTextField();
		tf1.setBounds(80, 11, 186, 25);
		contentPane.add(tf1);
		tf1.setColumns(10);

		btn1.setBounds(280, 12, 112, 23);
		contentPane.add(btn1);

		tf2 = new JTextField();
		tf2.setBounds(80, 60, 186, 22);
		contentPane.add(tf2);
		tf2.setColumns(10);

		btn2.setBounds(280, 60, 112, 23);
		contentPane.add(btn2);

		JLabel lb1 = new JLabel("polinom1:");
		lb1.setBounds(23, 16, 63, 14);
		contentPane.add(lb1);

		JLabel lb2 = new JLabel("polinom2:");
		lb2.setBounds(23, 64, 78, 14);
		contentPane.add(lb2);

		btnDerivare = new JButton("Derivare");
		btnDerivare.setBounds(203, 175, 89, 23);
		contentPane.add(btnDerivare);

		btnAdunare = new JButton("Adunare");
		btnAdunare.setBounds(10, 175, 89, 23);
		contentPane.add(btnAdunare);

		btnScadere = new JButton("Scadere");
		btnScadere.setBounds(109, 175, 89, 23);
		contentPane.add(btnScadere);

		btnR1 = new JButton("Reset");
		btnR1.setBounds(402, 27, 99, 43);
		contentPane.add(btnR1);

		check1 = new JCheckBox("");
		check1.setBounds(2, 11, 21, 23);
		contentPane.add(check1);

		check2 = new JCheckBox("");
		check2.setBounds(2, 60, 21, 23);
		contentPane.add(check2);

		btnInmultire = new JButton("Inmultire");
		btnInmultire.setBounds(10, 223, 89, 23);
		contentPane.add(btnInmultire);

		tfRezultat = new JTextField();
		tfRezultat.setBounds(80, 126, 186, 20);
		contentPane.add(tfRezultat);
		tfRezultat.setColumns(10);

		JLabel lbRezultat = new JLabel("Rezultat:");
		lbRezultat.setBounds(10, 129, 60, 14);
		contentPane.add(lbRezultat);

		btnIntegrare = new JButton("Integrare");
		btnIntegrare.setBounds(203, 223, 89, 23);
		contentPane.add(btnIntegrare);

		btnImpartire = new JButton("Impartire");
		btnImpartire.setBounds(109, 223, 89, 23);
		contentPane.add(btnImpartire);
		
		tfRest = new JTextField();
		tfRest.setBounds(343, 126, 139, 20);
		contentPane.add(tfRest);
		tfRest.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Rest");
		lblNewLabel.setBounds(297, 129, 31, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(381, 209, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		btnF = new JButton("f(x)");
		btnF.setBounds(378, 175, 89, 23);
		contentPane.add(btnF);
	}

	public void addBtn1(ActionListener e) {
		btn1.addActionListener(e);
	}

	public void addBtn2(ActionListener e) {
		btn2.addActionListener(e);
	}

	public void addBtnDerivare(ActionListener e) {
		btnDerivare.addActionListener(e);
	}

	public void addBtnIntegrare(ActionListener e) {
		btnIntegrare.addActionListener(e);
	}

	public void addBtnAdunare(ActionListener e) {
		btnAdunare.addActionListener(e);
	}

	public void addBtnScadere(ActionListener e) {
		btnScadere.addActionListener(e);
	}

	public void addBtnReset1(ActionListener e) {
		btnR1.addActionListener(e);
	}

	public void addBtnInmultire(ActionListener e) {
		btnInmultire.addActionListener(e);
	}

	public void addBtnImpartire(ActionListener e) {
		btnImpartire.addActionListener(e);
	}

	public void addBtnF(ActionListener e) {
		btnF.addActionListener(e);
	}
	
	public String getPolinom1() {
		return tf1.getText();
	}

	public String getPolinom2() {
		return tf2.getText();
	}

	public void setPolinom1(String newText) {
		tf1.setText(newText);
	}

	public void setRezultat(String newText) {
		tfRezultat.setText(newText);
	}

	public int getF(){
		return Integer.parseInt(textField.getText());
	}
	public void setF(String newText){
		textField.setText(newText);
	}
	public void setRest(String newText) {
		tfRest.setText(newText);
	}
	
	public void setPolinom2(String newText) {
		tf2.setText(newText);
	}

	public boolean isCheckBox1() {
		return check1.isSelected();
	}

	public boolean isCheckBox2() {
		return check2.isSelected();
	}
}

package Model;

public class Monom implements Comparable<Monom> {
	private float coef;
	private int putere;

	public Monom(float coef, int putere) {
		this.setCoef(coef);
		this.setPutere(putere);
	}

	public float getCoef() {
		return coef;
	}

	public int getPutere() {
		return putere;
	}

	public void setCoef(float coef) {
		this.coef = coef;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	public int compareTo(Monom m) {
		int c = m.getPutere();
		return c - this.getPutere();
	}

}



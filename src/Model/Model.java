package Model;

import java.util.Scanner;

public class Model {
	private Polinom rezultat = new Polinom();
	private Polinom polinom1 = new Polinom();
	private Polinom polinom2 = new Polinom();
	private Polinom rest= new Polinom();



	public String functie(int nr,int x){
		if(x==0){
			return polinom1.f(nr);
		}else if(x==1){
			return polinom2.f(nr);
		}
		return null;
	}
	public void setPolinom1(String text) {
		polinom1 = new Polinom();
		polinom1.conversie(text);
		polinom1.afisare();
		polinom1.sortare();
	}

	public void setPolinom2(String text) {
		polinom2 = new Polinom();
		polinom2.conversie(text);

		polinom2.afisare();
		polinom2.sortare();
	}

	public String getPolinom1() {
		return polinom1.getPolinom();
	}

	public String getPolinom2() {
		return polinom2.getPolinom();
	}

	public String getRezultat() {
		return rezultat.getPolinom();
	}
	
	public String getRest(){
		if(rest==null)
			return "operatie imposibila";
		
		return rest.getPolinom();
	}

	public void adunare() {
		rezultat = new Polinom();
		rezultat.adunare(polinom1, polinom2);
	}

	public void inmultire() {
		rezultat = new Polinom();
		this.rezultat.inmultire(polinom1, polinom2);
	}

	public void impartire() {
		rezultat = new Polinom();
		rest=this.rezultat.impartire(polinom1, polinom2);
	}

	public void scadere() {
		rezultat = new Polinom();
		this.rezultat.scadere(polinom1, polinom2);
	}

	public void derivare(int x) {

		if (x == 0) {
			this.polinom1.derivare();
		} else if (x == 1)
			this.polinom2.derivare();
	}

	public void integrare(int x) {

		if (x == 0) {
			this.polinom1.integrare();
		} else if (x == 1)
			this.polinom2.integrare();
	}

	public Polinom getPol1() {
		return polinom1;
	}

	public Polinom getPol2() {
		return polinom2;
	}

}

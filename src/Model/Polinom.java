package Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Polinom {
	private ArrayList<Monom> pol;

	public Polinom() {
		pol = new ArrayList<Monom>();

	}

	public void afisare() {
		Monom x;
		for (int i = 0; i < pol.size(); i++) {
			x = pol.get(i);
			if (x.getCoef() < 0) {

				System.out.printf("%fx^%d ", x.getCoef(), x.getPutere());
			} else
				System.out.printf("+%fx^%d ", x.getCoef(), x.getPutere());
		}
		System.out.println();
	}

	public void adunare(Polinom p1, Polinom p2) {

		p1.afisare();
		for (int i = 0; i < p1.pol.size(); i++)
			pol.add(new Monom(p1.pol.get(i).getCoef(), p1.pol.get(i).getPutere()));
		for (int i = 0; i < p2.pol.size(); i++) {
			pol.add(new Monom(p2.pol.get(i).getCoef(), p2.pol.get(i).getPutere()));
		}
		this.sortare();
		int i = 0;
		while (i < this.pol.size() - 1) {
			if (this.pol.get(i).getPutere() == this.pol.get(i + 1).getPutere()) {
				this.pol.get(i).setCoef(this.pol.get(i).getCoef() + this.pol.get(i + 1).getCoef());
				this.pol.remove(i + 1);
				if (this.pol.get(i).getCoef() == 0) {
					this.pol.remove(i);
				} else
					i++;

			} else
				i++;

		}
	}

	public void scadere(Polinom p1, Polinom p2) {
		for (int i = 0; i < p1.pol.size(); i++) {
			this.pol.add(new Monom(p1.pol.get(i).getCoef(), p1.pol.get(i).getPutere()));
		}

		for (int i = 0; i < p2.pol.size(); i++) {
			this.pol.add(new Monom((-1) * p2.pol.get(i).getCoef(), p2.pol.get(i).getPutere()));
		}
		this.sortare();
		this.afisare();
		int i = 0;

		while (i < this.pol.size() - 1) {
			if (this.pol.get(i).getPutere() == this.pol.get(i + 1).getPutere()) {
				this.pol.get(i).setCoef(this.pol.get(i).getCoef() + this.pol.get(i + 1).getCoef());
				this.pol.remove(i + 1);
				if (this.pol.get(i).getCoef() == 0) {
					this.pol.remove(i);
				} else
					i++;

			} else
				i++;

		}

	}

	public void derivare() {
		for (int i = 0; i < this.pol.size(); i++) {
			if (this.pol.get(i).getPutere() == 0)
				this.pol.remove(i);
			else {
				this.pol.get(i).setCoef(this.pol.get(i).getCoef() * this.pol.get(i).getPutere());
				this.pol.get(i).setPutere(this.pol.get(i).getPutere() - 1);
			}
		}
	}

	public void conversie(String linie) {
		int x, y;
		for (int i = 0; i < this.pol.size(); i++)
			this.pol.remove(i);

		for (int i = 0; i < linie.length();) {
			x = 0;
			y = 0;

			boolean negative = false;

			while (i < linie.length() && linie.charAt(i) != '-' && (linie.charAt(i) < '0' || linie.charAt(i) > '9'))
				i++;
			if (linie.charAt(i) == '-') {
				negative = true;
				i++;
			} else if (linie.charAt(i) == '+')
				i++;
			while (i < linie.length() && (linie.charAt(i) < '0' || linie.charAt(i) > '9'))
				i++;
			while (i < linie.length() && linie.charAt(i) >= '0' && linie.charAt(i) <= '9') {
				x = x * 10 + linie.charAt(i) - 48;
				i++;
			}
			if (negative == true) {
				x = x * (-1);
			}
			if (i < linie.length()) {
				if (linie.charAt(i) != 'x') {
					this.pol.add(new Monom(x, 0));
					break;
				} else {
					negative = false;
					i = i + 2;
					while (i < linie.length() && (linie.charAt(i) < '0' || linie.charAt(i) > '9')
							&& linie.charAt(i) != '+' && linie.charAt(i) != '-')
						i++;
					// System.out.println("hi");
					if (linie.charAt(i) == '-') {
						negative = true;
						i++;
					}
					while (i < linie.length() && (linie.charAt(i) < '0' || linie.charAt(i) > '9'))
						i++;
					while (i < linie.length() && linie.charAt(i) >= '0' && linie.charAt(i) <= '9') {
						y = y * 10 + linie.charAt(i) - 48;
						i++;
					}
					if (negative == true)
						y = y * (-1);
					this.pol.add(new Monom(x, y));

				}
			} else if (x != 0)
				this.pol.add(new Monom(x, 0));

		}

	}

	public void sortare() {
		Collections.sort(this.pol);
	}

	public String getPolinom() {
		Monom x;
		String text = "";
		for (int i = 0; i < pol.size(); i++) {
			x = pol.get(i);
			if (x.getPutere() == 0) {
				if (x.getCoef() < 0) {

					text = text + x.getCoef();
				} else if (i != 0) {
					text = text + "+" + x.getCoef();
				} else
					text = text + x.getCoef();

			} else {
				if (x.getCoef() < 0) {

					text = text + x.getCoef() + "x^" + x.getPutere();
				} else if (i != 0) {
					text = text + "+" + x.getCoef() + "x^" + x.getPutere();
				} else
					text = text + x.getCoef() + "x^" + x.getPutere();

			}
		}
		return text;
	}

	public void inmultire(Polinom p1, Polinom p2) {
		int size = p1.pol.size();
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < p2.pol.size(); j++) {
				this.pol.add(new Monom(p1.pol.get(i).getCoef() * (p2.pol.get(j).getCoef()),
						p1.pol.get(i).getPutere() + p2.pol.get(j).getPutere()));
			}
		}
		p1.afisare();
		p2.afisare();
		this.sortare();
		for (int i = 0; i < this.pol.size() - 1; i++)
			if (this.pol.get(i).getPutere() == this.pol.get(i + 1).getPutere()) {
				this.pol.get(i).setCoef(this.pol.get(i).getCoef() + this.pol.get(i + 1).getCoef());
				this.pol.remove(i + 1);
			}
		for (int i = 0; i < this.pol.size(); i++) {
			System.out.println(this.pol.get(i).getCoef() + "x^" + this.pol.get(i).getPutere());
		}
	}

	public Polinom inmultireMonom(Polinom p1, Monom m) {
		Polinom aux = new Polinom();
		for (int i = 0; i < p1.pol.size(); i++)
			aux.pol.add(new Monom(p1.pol.get(i).getCoef() * m.getCoef(), p1.pol.get(i).getPutere() + m.getPutere()));

		return aux;
	}

	public Polinom impartire(Polinom p1, Polinom p2) {
		Polinom aux1 = new Polinom();
		Polinom aux2 = new Polinom();

		for (int i = 0; i < p1.pol.size(); i++)
			aux1.pol.add(new Monom(p1.pol.get(i).getCoef(), p1.pol.get(i).getPutere()));
		for (int i = 0; i < p2.pol.size(); i++) {
			aux2.pol.add(new Monom(p2.pol.get(i).getCoef(), p2.pol.get(i).getPutere()));
		}

		if (aux2.pol.isEmpty() == true)
			return null;
		else

			while (aux1.pol.isEmpty() != true && aux1.pol.get(0).getPutere() >= aux2.pol.get(0).getPutere()) {

				this.pol.add(new Monom(aux1.pol.get(0).getCoef() / (aux2.pol.get(0).getCoef()),
						aux1.pol.get(0).getPutere() - aux2.pol.get(0).getPutere()));

				aux1.pol.remove(0);

				int size = aux2.pol.size();
				// this.sortare();
				if (aux1.pol.isEmpty() != true)
					for (int i = 1; i < size;) {
						System.out.println(size + " " + aux1.pol.size() + " " + i);
						aux1.pol.add(new Monom((-1) * aux2.pol.get(i).getCoef() * pol.get(pol.size() - 1).getCoef(),
								aux2.pol.get(i).getPutere() + pol.get(pol.size() - 1).getPutere()));
						i++;
					}

				aux1.sortare();
				int i = 0;

				while (i < aux1.pol.size() - 1) {
					if (aux1.pol.get(i).getPutere() == aux1.pol.get(i + 1).getPutere()) {
						aux1.pol.get(i).setCoef(aux1.pol.get(i).getCoef() + aux1.pol.get(i + 1).getCoef());
						aux1.pol.remove(i + 1);
						if (aux1.pol.get(i).getCoef() == 0) {
							aux1.pol.remove(i);
						} else
							i++;

					} else
						i++;

				}
			}
		return aux1;
	}

	public void integrare() {
		for (int i = 0; i < this.pol.size(); i++) {
			this.pol.get(i).setCoef(this.pol.get(i).getCoef() / (this.pol.get(i).getPutere() + 1));
			this.pol.get(i).setPutere(this.pol.get(i).getPutere() + 1);
		}
	}

	public String f(int nr) {
		float rezultat = 0;
		for (int i = 0; i < this.pol.size(); i++) {
			float aux = 1;
			for (int j = 0; j < this.pol.get(i).getPutere(); j++)
				aux = aux * nr;
			rezultat = rezultat + aux * this.pol.get(i).getCoef();

		}

		return String.valueOf(rezultat);

	}

}
